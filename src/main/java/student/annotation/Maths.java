package student.annotation;

import org.springframework.stereotype.Component;

@Component
public class Maths implements Subject {

    private final String NAME = "Mathematics";

    @Override
    public String getName() {
        return NAME;
    }
}
