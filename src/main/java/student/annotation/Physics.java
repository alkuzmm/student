package student.annotation;

import org.springframework.stereotype.Component;

@Component
public class Physics implements Subject {

    private final String NAME = "Physics";

    @Override
    public String getName() {
        return NAME;
    }
}
