package student.annotation;

public interface Teacher {
    String getName();
}
