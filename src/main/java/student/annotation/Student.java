package student.annotation;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.ComponentScan;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ComponentScan(basePackages = "student.annotation")
@Data
@NoArgsConstructor
public class Student {

    private String name;

    private Integer age;

    private Teacher teacher;

    private List<String> subjects = new ArrayList<>();

    private Map<String, Integer> marksMap = new HashMap<>();


    public void setMarks(String name, Integer mark) {
        marksMap.put(name, mark);
    }

    public void setSubject(String name) {
        subjects.add(name);
    }

    public void study() {
        System.out.println(String.format("I study with teacher %s these subjects :", teacher.getName()));
        subjects.forEach(System.out::println);
        System.out.println("And have these marks: ");
        marksMap.forEach((k, v) -> System.out.println(String.format("Subject: %s, Mark: %s", k, v)));
    }
}
