package student.annotation;

import org.springframework.stereotype.Component;

@Component
public class TeacherImpl implements Teacher {

    private final String NAME = "Mykola Ivanovych";

    @Override
    public String getName() {
        return NAME;
    }
}
