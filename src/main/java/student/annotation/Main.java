package student.annotation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

    public static void main(String[] args) {

        ApplicationContext ctx = new AnnotationConfigApplicationContext(Student.class);
        Student student = ctx.getBean(Student.class);
        student.setName("Oleksandr");
        student.setAge(36);
        Subject math = new Maths();
        Subject phys = new Physics();
        Teacher teacher = new TeacherImpl();
        student.setTeacher(teacher);
        student.setMarks(math.getName(), 5);
        student.setMarks(phys.getName(), 5);
        student.setSubject(math.getName());
        student.setSubject(phys.getName());
        student.study();
    }
}
