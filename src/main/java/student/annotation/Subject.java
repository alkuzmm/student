package student.annotation;

public interface Subject {
    String getName();
}
