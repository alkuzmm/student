package student.xml;

import java.util.List;
import java.util.Map;

public class Student {

    private String name;

    private Integer age;

    private Teacher teacher;

    private List<Subject> subjects;

    private Map<String, Integer> marksMap;

    public Student(String name, Integer age, Teacher teacher, List<Subject> subjects, Map<String, Integer> marksMap) {
        this.name = name;
        this.age = age;
        this.teacher = teacher;
        this.subjects = subjects;
        this.marksMap = marksMap;
    }

    public Student() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    public Map<String, Integer> getMarksMap() {
        return marksMap;
    }

    public void setMarksMap(Map<String, Integer> marksMap) {
        this.marksMap = marksMap;
    }

    public void study() {
        System.out.println(String.format("I study with teacher %s these subjects :", teacher.getName()));
        subjects.forEach(subject -> System.out.println(subject.getName()));
        System.out.println("And have these marks: ");
        marksMap.forEach((k, v) -> System.out.println(String.format("Subject: %s, Mark: %s", k, v)));
    }
}
