package student.xml;

import org.springframework.stereotype.Component;

public class Maths implements Subject {

    private final String NAME = "Mathematics";

    @Override
    public String getName() {
        return NAME;
    }
}
