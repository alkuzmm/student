package student.xml;

public interface Subject {
    String getName();
}
