package student.xml;


public class Physics implements Subject {

    private final String NAME = "Physics";

    @Override
    public String getName() {
        return NAME;
    }
}
